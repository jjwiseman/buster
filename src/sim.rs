//! Connects to the flight simulator and gets simulator data.

use super::errors::Error;
use simconnect::{self, SimConnector};
use std::mem::transmute_copy;

/// This holds all the information we get from simconnect for the player's
/// aircraft. Note that we need both barometric altitude (AKA MSL, Mean Sea
/// Level, or altitude above sea level) and altitude above ground level (AGL)
/// because airspaces can be defined using MSL or AGL.

#[derive(Clone, Debug, PartialEq)]
pub struct SimPlaneState {
    /// Latitude.
    pub lat: f64,
    /// Longitude.
    pub lon: f64,
    /// Barometric altitude above sea level.
    pub alt: f64,
    /// Altitude above ground level.
    pub agl: f64,
}

/// A little macro that makes checking simconnect calls for success a little
/// nicer.

#[macro_export]
macro_rules! sc_success {
    ($hr:expr) => {
        if !$hr {
            Err(Error("simconnect call failed".into()))
        } else {
            Ok(())
        }
    };
}

/// Connect to the simulator and register all the sim variables we want to
/// track. Returns `Err` if it can't connect.

pub fn connect_to_sim(conn: &mut SimConnector) -> Result<(), Error> {
    sc_success!(conn.connect("Simple Program"))?;
    println!("Connected to sim");
    register_sim_vars(&conn).unwrap();
    Ok(())
}

/// Register the sim variables we want to track.
fn register_sim_vars(conn: &SimConnector) -> Result<(), Error> {
    sc_success!(conn.add_data_definition(
        0,
        "PLANE LATITUDE",
        "Degrees",
        simconnect::SIMCONNECT_DATATYPE_SIMCONNECT_DATATYPE_FLOAT64,
        u32::MAX,
    ))?;
    sc_success!(conn.add_data_definition(
        0,
        "PLANE LONGITUDE",
        "Degrees",
        simconnect::SIMCONNECT_DATATYPE_SIMCONNECT_DATATYPE_FLOAT64,
        u32::MAX,
    ))?;
    sc_success!(conn.add_data_definition(
        0,
        "PLANE ALTITUDE",
        "Feet",
        simconnect::SIMCONNECT_DATATYPE_SIMCONNECT_DATATYPE_FLOAT64,
        u32::MAX,
    ))?;
    sc_success!(conn.add_data_definition(
        0,
        "PLANE ALT ABOVE GROUND",
        "Feet",
        simconnect::SIMCONNECT_DATATYPE_SIMCONNECT_DATATYPE_FLOAT64,
        u32::MAX,
    ))?;
    sc_success!(conn.request_data_on_sim_object(
        0,
        0,
        0,
        simconnect::SIMCONNECT_PERIOD_SIMCONNECT_PERIOD_SIM_FRAME,
        0,
        0,
        0,
        0,
    ))?;
    Ok(())
}

/// Fetch the player aircraft's current state.
///
/// If there is no new state message _or_ if the connection has closed, this
/// will return `Err`.
pub fn get_sim_plane_state(conn: &SimConnector) -> Result<Option<SimPlaneState>, Error> {
    unsafe {
        let msg = conn.get_next_message()?;
        match msg {
            // Getting this result means the message queue is now empty.
            simconnect::DispatchResult::Null => Ok(None),
            simconnect::DispatchResult::Exception(exc) => {
                println!("sim: got exception {:?}", exc);
                Err(Error(format!("get_next_message exception: {:?}", exc)))
            }
            simconnect::DispatchResult::SimobjectData(data) => match data.dwDefineID {
                0 => {
                    let sim_data: SimPlaneState = transmute_copy(&data.dwData);
                    Ok(Some(sim_data))
                }
                // This doesn't seem to ever happen.
                _ => {
                    println!("sim wrong data: {}", data.dwDefineID);
                    Ok(None)
                }
            },
            _ => {
                // println!("Got another event: {:?}", msg);
                Ok(None)
            }
        }
    }
}
