//! Builds and manages the user interface, both audio and visual.

use super::tracker::Tracker;
use crate::tracker::TrackerEvent;
use druid::widget::{prelude::*, Controller};
use druid::{
    widget::{Flex, Label},
    Lens, Selector,
};
use druid::{Widget, WidgetExt};
use openaip::Airspace;

#[derive(Debug, Clone, Data, Lens)]
pub struct AppState {
    pub connected: bool,
    pub main_label: String,
    pub altitude: String,
    pub position: String,
}

pub const WINDOW_SIZE: (f64, f64) = (350., 150.);
const AIRSPACE_LABEL_SIZE: f64 = 20.;

#[derive(Debug, PartialOrd, PartialEq, Clone)]
pub struct AirspaceDesc {
    pub name: String,
    pub bottom: String,
    pub top: String,
}

impl AirspaceDesc {
    pub fn from_airspace(airspace: &Airspace) -> AirspaceDesc {
        AirspaceDesc {
            name: airspace.name.clone(),
            bottom: format!("{}", airspace.bottom.value),
            top: format!("{}", airspace.top.value),
        }
    }
}

#[derive(Debug, PartialOrd, PartialEq, Clone)]
pub enum AirspaceEvent {
    Entered(AirspaceDesc),
    Exited(AirspaceDesc),
}

pub const AIRSPACE_EVT: Selector<AirspaceEvent> = Selector::new("buster.airspace-event");

#[derive(Debug, PartialOrd, PartialEq, Clone)]
pub enum ConnectionEvent {
    Connected,
    Disconnected,
}

pub const CONNECTION_EVT: Selector<ConnectionEvent> = Selector::new("buster.connection-event");

#[derive(Debug, PartialOrd, PartialEq, Clone)]
pub struct PositionEvent {
    pub lat: f64,
    pub lon: f64,
}

pub const POSITION_EVT: Selector<PositionEvent> = Selector::new("buster.position-event");

impl AirspaceEvent {
    pub fn from_tracker_event(tracker: &Tracker, event: &TrackerEvent) -> AirspaceEvent {
        match event {
            TrackerEvent::Entered(id) => {
                let airspace = tracker.get_airspace(&id).unwrap();
                AirspaceEvent::Entered(AirspaceDesc::from_airspace(airspace))
            }
            TrackerEvent::Exited(id) => {
                let airspace = tracker.get_airspace(&id).unwrap();
                AirspaceEvent::Exited(AirspaceDesc::from_airspace(airspace))
            }
        }
    }
}

pub fn build_root_widget() -> impl Widget<AppState> {
    // The label text will be computed dynamically based on the current locale and count
    let airspace = Label::dynamic(|data: &AppState, _| {
        if data.connected {
            data.main_label.clone()
        } else {
            "DISCONNECTED".into()
        }
    })
    .with_text_size(AIRSPACE_LABEL_SIZE)
    // .lens(AppState::kount)
    .controller(AppWidget)
    .padding(5.)
    .center();
    let altitude = Label::dynamic(|data: &AppState, _| data.altitude.clone())
        .with_text_size(AIRSPACE_LABEL_SIZE / 1.2)
        // .padding(5.)
        .center();
    let position = Label::dynamic(|data: &AppState, _| data.position.clone())
        .with_text_size(AIRSPACE_LABEL_SIZE / 2.25)
        // .padding(5.)
        .center();
    Flex::column()
        .with_child(airspace)
        .with_child(altitude)
        .with_flex_child(position.expand_height(), 2.0)
}

struct AppWidget;

impl<W: Widget<AppState>> Controller<AppState, W> for AppWidget {
    fn event(
        &mut self,
        child: &mut W,
        ctx: &mut EventCtx,
        event: &Event,
        data: &mut AppState,
        env: &Env,
    ) {
        //println!("Got event: {:?}", event);
        match event {
            // This is where we handle our command.
            Event::Command(cmd) if cmd.is(AIRSPACE_EVT) => {
                // We don't do much data processing in the `event` method.
                // All we really do is just set the data. This causes a call
                // to `update` which requests a paint. You can also request a paint
                // during the event, but this should be reserved for changes to self.
                // For changes to `Data` always make `update` do the paint requesting.
                let payload = cmd.get_unchecked(AIRSPACE_EVT);
                match payload {
                    AirspaceEvent::Entered(ad) => {
                        data.main_label = ad.name.clone();
                        data.altitude = format!("{} to {}", ad.bottom, ad.top);
                    }
                    AirspaceEvent::Exited(_) => {
                        data.main_label = "".into();
                        data.altitude = "".into();
                    }
                }
            }
            Event::Command(cmd) if cmd.is(CONNECTION_EVT) => {
                let payload = cmd.get_unchecked(CONNECTION_EVT);
                match payload {
                    ConnectionEvent::Connected => {
                        data.connected = true;
                        data.main_label = "".to_string();
                        data.altitude = "".to_string();
                        data.position = "".to_string();
                    }
                    ConnectionEvent::Disconnected => {
                        data.connected = false;
                        data.main_label = "".to_string();
                        data.altitude = "".to_string();
                        data.position = "".to_string();
                    }
                }
            }
            Event::Command(cmd) if cmd.is(POSITION_EVT) => {
                let payload = cmd.get_unchecked(POSITION_EVT);
                data.position = format!("{:.6} {:.6}", payload.lat, payload.lon);
            }
            _ => (),
        }
        // let pre_data = data.raw.to_owned();
        // child.event(ctx, event, data, env);
        // if !data.raw.same(&pre_data) {
        //     data.rendered = rebuild_rendered_text(&data.raw);
        // }
        child.event(ctx, event, data, env);
    }
}
