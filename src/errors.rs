//! The error handling in this application is very simple: we collapse
//! everything into a string.

use std::{
    fmt::{self, Display, Formatter},
    io,
};

/// Our singular error representation.
#[derive(Debug)]
pub struct Error(pub String);

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl std::error::Error for Error {}

impl From<io::Error> for Error
where
    io::Error: std::fmt::Debug,
{
    fn from(error: io::Error) -> Self {
        Error(format!("{:?}", error))
    }
}

impl From<openaip::Error> for Error
where
    io::Error: std::fmt::Debug,
{
    fn from(error: openaip::Error) -> Self {
        Error(format!("{:?}", error))
    }
}

// simconnect errors are just &str.

impl From<&str> for Error {
    fn from(error: &str) -> Self {
        Error(error.to_string())
    }
}
