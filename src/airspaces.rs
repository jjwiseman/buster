//! Loads OpenAIP airspace data into a spatial database that can be queried
//! in two dimensions (latitute and longitude) or three dimensions (latitude,
//! longitude, altitude).
//!
//! ```
//! use buster::airspaces::{AirspaceIndex, load_airspace_data};
//! use geo::Coordinate;
//!
//! let mut index = AirspaceIndex::new();
//! let airspaces = load_airspace_data("openaip_airspace_united_states_us.aip").unwrap();
//! index.add_airspaces(airspaces);
//! let in_airspaces = index.get_at_coord(&Coordinate {
//!   x: -118.3572,
//!   y: 34.2013,
//! });
//!
//! assert_eq!(in_airspaces.len(), 1);
//! assert_eq!(in_airspaces[0].name, "BURBANK CLASS C".to_string());
//! ```
use super::errors::Error;
use core::f64;
use geo::{contains::Contains, prelude::BoundingRect, Coordinate, LineString, Polygon};
use openaip::{AltitudeLimit};
use rstar::{PointDistance, RTree, RTreeObject, AABB};
use std::fs::read_to_string;

/// An `openaip::Airspace`.
pub use openaip::Airspace;

/// This is an OpenAIP `Airspace` along with a corresponding `geo::Polygon` that
/// makes it easy to check if a 2D point is located in the airspace.
#[derive(Debug)]
struct BoundedAirspace {
    pub airspace: Airspace,
    pub polygon: Polygon<f64>,
}

/// We implement `RTreeObject` so that we can insert `BoundedAirspace` in an
/// `RTree`.
impl RTreeObject for BoundedAirspace {
    type Envelope = AABB<Coordinate<f64>>;

    /// The envelope is an 2D axis-aligned bounding box containing the airspace
    /// polygon.
    fn envelope(&self) -> Self::Envelope {
        let bounding_rect = self.polygon.bounding_rect().unwrap();
        AABB::from_corners(bounding_rect.min(), bounding_rect.max())
    }
}

impl PointDistance for BoundedAirspace {
    fn contains_point(&self, point: &Coordinate<f64>) -> bool {
        self.polygon.contains(point)
    }

    /// This method doesn't seem to be required for the way we query the
    /// `RTree`.
    fn distance_2(&self, _point: &Coordinate<f64>) -> f64 {
        todo!();
    }
}

impl BoundedAirspace {
    /// Creates a new `BoundedAirspace` from an `openaip::Airspace`.
    pub fn new(airspace: Airspace) -> BoundedAirspace {
        let polygon = open_aip_geometry_to_geo_polygon(&airspace.geometry);
        BoundedAirspace { airspace, polygon }
    }
}

/// An `AirspaceIndex` is a spatial index for airspaces that supports very quick
/// lookups of which airspaces contain a 2D or 3D point.
#[derive(Debug)]
pub struct AirspaceIndex {
    index: RTree<BoundedAirspace>,
}

/// A thing that has both a Mean Sea Level (MSL) altitude, or altitude above sea
/// level, and Above Ground Level (AGL) altitude.
pub trait Altitude {
    /// Returns the Mean Sea Level altitude, in feet.
    fn msl(&self) -> f64;
    /// Returns the Above Ground Level altitude, in feet.
    fn agl(&self) -> f64;
}

/// This gets the relevant altitude from an entity to compare against an
/// airspace's `AltitudeLimit`. For example, if the altitude limit is defined
/// relative to the ground, this will return the enity's AGL.
fn relevant_alt<T>(entity: &T, limit: &AltitudeLimit) -> f64
where
    T: Altitude,
{
    match limit.reference {
        openaip::AltitudeReference::GND => entity.agl(),
        openaip::AltitudeReference::MSL => entity.msl(),
        openaip::AltitudeReference::STD => entity.msl(),
    }
}

fn normalize_altitude_limit_value(limit: &AltitudeLimit) -> i32 {
    match limit.unit {
        openaip::AltitudeUnit::Feet => limit.value,
        openaip::AltitudeUnit::FlightLevel => limit.value * 100,
    }
}

pub fn cmp_alt_limit<T>(entity: &T, limit: &AltitudeLimit) -> AltitudeRelation
where
    T: Altitude,
{
    let entity_alt = relevant_alt(entity, limit);
    let limit_alt = normalize_altitude_limit_value(limit) as f64;
    if entity_alt >= limit_alt {
        AltitudeRelation::Above(entity_alt - limit_alt)
    } else {
        AltitudeRelation::Below(limit_alt - entity_alt)
    }
}

impl Default for AirspaceIndex {
    fn default() -> AirspaceIndex {
        AirspaceIndex::new()
    }
}

impl AirspaceIndex {
    /// Creates a new, empty `AirspaceIndex`.
    pub fn new() -> AirspaceIndex {
        AirspaceIndex {
            index: RTree::new(),
        }
    }

    /// Adds a collection of `openaip::Airspace` to the index.
    pub fn add_airspaces(&mut self, airspaces: Vec<Airspace>) {
        for a in airspaces {
            // TODO: There must be a way to use RTree::bulk_load here, but I
            // can't figure out how to make the ownership work. Not using
            // bulk_load seems plenty fast, though.
            self.index.insert(BoundedAirspace::new(a));
        }
    }

    /// Find all the airspaces that contain a 2D coordinate.
    ///
    /// This will return airspaces at all altitudes for a given lat, lon
    /// coordinate.
    pub fn get_at_coord(&self, point: &Coordinate<f64>) -> Vec<&Airspace> {
        let mut airspaces: Vec<&Airspace> = vec![];
        for ba in self.index.locate_all_at_point(point) {
            airspaces.push(&ba.airspace);
        }
        airspaces
    }

    /// Find all the airspaces that contain a 3D coordinate.
    pub fn get_at_coord_alt<T>(&self, point: &Coordinate<f64>, alt: &T) -> Vec<&Airspace>
    where
        T: Altitude,
    {
        let airspaces: Vec<&Airspace> = self
            .index
            .locate_all_at_point(point)
            .filter(|a| {
                let bottom_alt = relevant_alt(alt, &a.airspace.bottom);
                let top_alt = relevant_alt(alt, &a.airspace.top);
                let airspace_bottom = normalize_altitude_limit_value(&a.airspace.bottom) as f64;
                let airspace_top = normalize_altitude_limit_value(&a.airspace.top) as f64;
                bottom_alt >= airspace_bottom && top_alt <= airspace_top
            })
            .map(|a| &a.airspace)
            .collect();
        airspaces
    }

    /// Looks up an airspace by its ID.
    pub fn get_by_id(&self, id: &str) -> Option<&Airspace> {
        for a in self.index.iter() {
            if a.airspace.id == id {
                return Some(&a.airspace);
            }
        }
        None
    }
}

/// Create a `geo::Polygon` corresponding to an `openaip::Geometry`.
///
/// We do this because `geo::Polygon` has a convenient `contains` method.
fn open_aip_geometry_to_geo_polygon(poly: &openaip::Geometry) -> Polygon<f64> {
    match poly {
        openaip::Geometry::Polygon(coords) => {
            let cs: Vec<Coordinate<f64>> = coords
                .iter()
                .map(|p| Coordinate {
                    x: p.longitude,
                    y: p.latitude,
                })
                .collect();
            Polygon::new(LineString::from(cs), vec![])
        }
    }
}

/// Loads an OpenAIP `.aip` file containing airspace definitions.
pub fn load_airspace_data(path: &str) -> Result<Vec<Airspace>, Error> {
    let content = read_to_string(path)?;
    let file = openaip::parse(&content)?;
    let airspaces: Vec<Airspace> = match file.airspaces {
        Some(airspace_results) => airspace_results.into_iter().collect(),
        _ => Ok(vec![]),
    }?;
    println!("Loaded {} airspaces from {}", airspaces.len(), path);
    Ok(airspaces)
}

/// Represents whether a position is in an airspace vs above or below, and how
/// far.
#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub enum AltitudeRelation {
    Below(f64),
    In,
    Above(f64),
}

/// A thing with a text description.
pub trait Describe {
    /// Returns the text description.
    fn describe(&self) -> String;
}

impl Describe for Airspace {
    fn describe(&self) -> String {
        format!(
            "{} from {} to {}",
            self.name, self.bottom.value, self.top.value
        )
    }
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_airspace_index() {
        let mut index = AirspaceIndex::new();
        let airspaces = load_airspace_data("openaip_airspace_united_states_us.aip").unwrap();
        index.add_airspaces(airspaces);
        let in_airspaces = index.get_at_coord(&Coordinate {
            x: -118.3572,
            y: 34.2013,
        });
        assert_eq!(in_airspaces.len(), 1);
        assert_eq!(in_airspaces[0].name, "BURBANK CLASS C".to_string());
    }
}
