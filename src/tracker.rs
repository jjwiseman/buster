//! Keeps track of an aircraft's airspace and airspace transitions.

use crate::{
    airspaces::{AirspaceIndex, Altitude},
    sim::SimPlaneState,
};
use geo::Coordinate;
use openaip::Airspace;

/// Keeps track of an aircraft's airspace state.
///
/// Besides efficiently looking up and keeping track of which airspace an
/// aircraft is in, this also generates airspace transition events.
///
/// To use a `Tracker`, you create a new one loaded with all possible airspaces.
/// You then update it every time the aircraft position changes. Each update may
/// change the current airspace, as well as generate airspace transition events.

#[derive(Debug)]
pub struct Tracker {
    index: AirspaceIndex,
    cur_airspace: Option<String>,
}

/// Represents a transition into or out of an airspace.
#[derive(Debug)]
pub enum TrackerEvent {
    Entered(String),
    Exited(String),
}

/// A thing with a 2D location.
pub trait Located {
    /// Returns the 2D coordinate.
    fn coord(&self) -> Coordinate<f64>;
}

impl Located for SimPlaneState {
    fn coord(&self) -> Coordinate<f64> {
        Coordinate {
            x: self.lon,
            y: self.lat,
        }
    }
}

impl Altitude for SimPlaneState {
    fn msl(&self) -> f64 {
        self.alt
    }

    fn agl(&self) -> f64 {
        self.agl
    }
}

impl<'a> Tracker {
    /// Creates a new `Tracker` from a collection of airspaces.
    ///
    /// The `Tracker` creates a spatial index of the airspaces to allow for
    /// efficient lookups of the airspace for a particular position.
    pub fn new(airspaces: Vec<Airspace>) -> Tracker {
        let mut index = AirspaceIndex::new();
        index.add_airspaces(airspaces);
        Tracker {
            index,
            cur_airspace: None,
        }
    }

    /// Resets the tracker.
    pub fn reset(&mut self) {
        self.cur_airspace = None;
    }

    /// Returns the current airspace.
    pub fn current_airspace(&self) -> Option<&Airspace> {
        match &self.cur_airspace {
            Some(id) => self.get_airspace(&id),
            None => None,
        }
    }

    /// Looks up an airspace by its ID.
    pub fn get_airspace(&self, id: &str) -> Option<&Airspace> {
        self.index.get_by_id(id)
    }

    /// Updates the tracker with a new position.
    ///
    /// Returns a vector of `TrackerEvent`s.
    pub fn update<T>(&mut self, plane: &T) -> Vec<TrackerEvent>
    where
        T: Located + Altitude,
    {
        let mut events = vec![];
        let coord = plane.coord();
        let airspaces = self.index.get_at_coord_alt(&coord, plane);
        // The OpenAIP polygons overlap a bit, so if we find that we're inside
        // more than one, just ignore the hits and hopefully by the next update
        // the ambiguity will resolve itself.
        let new_airspace = if airspaces.len() == 1 {
            Some(airspaces[0])
        } else {
            None
        };
        match (&self.cur_airspace, new_airspace) {
            (Some(cur), Some(new)) => {
                if *cur != new.id {
                    events.push(TrackerEvent::Entered(new.id.clone()));
                    self.cur_airspace = Some(new.id.clone());
                }
            }
            (None, Some(new)) => {
                events.push(TrackerEvent::Entered(new.id.clone()));
                self.cur_airspace = Some(new.id.clone());
            }
            (Some(cur), None) => {
                events.push(TrackerEvent::Exited(cur.clone()));
                self.cur_airspace = None;
            }
            _ => {}
        }
        events
    }
}

#[cfg(test)]
mod tests {
    use crate::airspaces::load_airspace_data;

    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_tracker_airspace() {
        let airspaces = load_airspace_data("openaip_airspace_united_states_us.aip").unwrap();
        let mut tracker = Tracker::new(airspaces);
        tracker.update(&SimPlaneState {
            lat: 34.2013,
            lon: -118.3572,
            alt: 0.,
            agl: 0.,
        });
        println!("{:?}", tracker.cur_airspace);
        let airspace = tracker.current_airspace().unwrap();
        assert_eq!(airspace.name, "BURBANK CLASS C".to_string());
    }
}
