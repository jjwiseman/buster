//! Hello, and welcome to Buster!
//!
//! This code is for a companion app to Microsoft Flight Simulator 2020 that
//! tells you which airspace you're in while you're flying.
//!
//! It uses text-to-speech to alert you whenever you enter a new airspace.

pub mod airspaces;
pub mod errors;
pub mod sim;
pub mod tracker;
pub mod tts;
pub mod ui;
