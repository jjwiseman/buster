// #![deny(rust_2018_idioms)]
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]
use buster::sim::get_sim_plane_state;
use buster::tracker::{Tracker, TrackerEvent};
use buster::tts::phoneticize;
use buster::ui::{
    build_root_widget, AirspaceEvent, ConnectionEvent, PositionEvent, AIRSPACE_EVT, CONNECTION_EVT,
    POSITION_EVT, WINDOW_SIZE,
};
use buster::{
    airspaces::{load_airspace_data, Describe},
    ui::AppState,
};
use buster::{errors::Error, sim::connect_to_sim};
use druid::{AppLauncher, ExtEventSink, Target, WindowDesc};
use glob::glob;
use openaip::Airspace;
use std::process;
use std::thread::sleep;
use std::time::Duration;
use std::{panic, thread};
use tts::Tts;

fn process_sim_messages(
    conn: &simconnect::SimConnector,
    event_sink: &ExtEventSink,
    tracker: &mut Tracker,
    tts: &mut Tts,
) -> Result<(), Error> {
    let mut plane_state = None;
    tracker.reset();
    let mut num_consecutive_errors = 0;
    let update_freq_hz = 60;
    let period = Duration::from_millis(1000 / update_freq_hz);
    let mut is_connected = false;
    let mut num_messages: u64 = 0;
    loop {
        match get_sim_plane_state(&conn) {
            Ok(Some(new_plane_state)) => {
                num_consecutive_errors = 0;
                num_messages += 1;
                if !is_connected && ((num_messages - num_consecutive_errors) > update_freq_hz) {
                    is_connected = true;
                    event_sink
                        .submit_command(CONNECTION_EVT, ConnectionEvent::Connected, Target::Auto)
                        .unwrap();
                }
                if is_connected {
                    let state_changed = match plane_state {
                        Some(p) => p != new_plane_state,
                        _ => true,
                    };
                    if state_changed {
                        event_sink
                            .submit_command(
                                POSITION_EVT,
                                PositionEvent {
                                    lat: new_plane_state.lat,
                                    lon: new_plane_state.lon,
                                },
                                Target::Auto,
                            )
                            .unwrap();
                        let events = tracker.update(&new_plane_state);
                        for event in events {
                            println!("Tracker event: {:?}", event);
                            match &event {
                                TrackerEvent::Entered(id) => {
                                    let airspace = tracker.get_airspace(&id.clone()).unwrap();
                                    let utt =
                                        format!("You just entered the {}", airspace.describe());
                                    println!("{}", utt);
                                    tts.speak(phoneticize(&utt), false).unwrap();
                                }
                                TrackerEvent::Exited(id) => {
                                    let airspace = tracker.get_airspace(&id.clone()).unwrap();
                                    let utt = format!("You just left the {}", airspace.describe());
                                    println!("{}", utt);
                                    // tts.speak(utt, false).unwrap();
                                }
                            }
                            let ui_event = AirspaceEvent::from_tracker_event(&tracker, &event);
                            println!("Sending event");
                            if event_sink
                                .submit_command(AIRSPACE_EVT, ui_event, Target::Auto)
                                .is_err()
                            {
                                println!("Got error");
                                break;
                            }
                            println!("Sent event");
                        }
                    }
                    plane_state = Some(new_plane_state);
                }
                eprint!(".");
            }
            Ok(None) => {
                num_consecutive_errors = 0;
                eprint!("N");
                sleep(period);
            }
            Err(_) => {
                num_consecutive_errors += 1;
                eprint!("E");
                if num_consecutive_errors > 5 * update_freq_hz {
                    event_sink
                        .submit_command(CONNECTION_EVT, ConnectionEvent::Disconnected, Target::Auto)
                        .unwrap();
                    return Err(Error("Too many errors".to_string()));
                } else {
                    sleep(period);
                }
            }
        }
    }
}

fn load_airspace_definitions() -> Vec<Airspace> {
    let mut all_airspaces = vec![];
    for entry in glob("*.aip").unwrap() {
        let mut airspaces = load_airspace_data(entry.unwrap().to_str().unwrap()).unwrap();
        all_airspaces.append(&mut airspaces);
    }
    all_airspaces
}

fn generate_events(event_sink: druid::ExtEventSink) {
    let airspaces = load_airspace_definitions();
    let mut tracker = Tracker::new(airspaces);
    let mut tts = Tts::default().unwrap();
    tts.speak("Bustin' makes me feel goooood.", false).unwrap();
    loop {
        let mut conn = simconnect::SimConnector::new();
        match connect_to_sim(&mut conn) {
            Ok(_) => {
                sleep(Duration::from_millis(2000));
                if let Err(e) = process_sim_messages(&conn, &event_sink, &mut tracker, &mut tts) {
                    println!("Error processing sim messages: {}", e);
                }
            }
            Err(_) => {
                thread::sleep(Duration::from_millis(500));
            }
        }
    }
}

fn main() {
    // If any thread panics, exit the process.
    let orig_hook = panic::take_hook();
    std::panic::set_hook(Box::new(move |panic_info| {
        orig_hook(panic_info);
        println!("Aborting");
        process::exit(1);
    }));

    let main_window = WindowDesc::new(build_root_widget())
        .window_size(WINDOW_SIZE)
        .title("Buster");
    // create the initial app state
    let initial_state = AppState {
        connected: false,
        main_label: "DISCONNECTED".into(),
        altitude: "".into(),
        position: "".into(),
    };

    // start the application
    let launcher = AppLauncher::with_window(main_window).log_to_console();
    let event_sink = launcher.get_external_handle();
    thread::spawn(move || generate_events(event_sink));
    launcher
        .launch(initial_state)
        .expect("Failed to launch application");
}
