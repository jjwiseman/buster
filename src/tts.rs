use lazy_static::lazy_static;
use regex::Regex;

pub fn phoneticize(name: &str) -> String {
    lazy_static! {
        static ref RE: Regex =
            Regex::new(r#"\b(:?([A-Z])|([A-Z]{1,2}[0-9][A-Z0-9]*)|([0-9]{1,3}))\b"#).unwrap();
    }
    let mut pieces = vec![];
    let mut start: usize = 0;
    for caps in RE.captures_iter(name) {
        // println!("{:?}", caps);
        let cap = caps.get(0).unwrap();
        // println!("  {:?}", cap);
        if cap.start() > start {
            pieces.push(name[start..cap.start()].to_string());
        }
        for p in alpc::parse((&name[cap.start()..cap.end()]).to_string()) {
            pieces.push(p.to_string());
        }
        start = cap.end();
    }
    if start < name.len() {
        pieces.push(name[start..].to_string());
    }
    pieces = pieces.iter().map(|p| p.trim().to_string()).collect();
    pieces.join(" ")
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_phoneticize() {
        assert_eq!(
            phoneticize("R2508 CONT 126.35"),
            "ROMEO TWO FIFE ZERO EIGHT CONT ONE TWO SIX . TREE FIFE".to_string()
        );
        assert_eq!(
            phoneticize("ABERDEEN CLASS E2"),
            "ABERDEEN CLASS ECHO TWO".to_string()
        );
        assert_eq!(
            phoneticize("ABERDEEN CLASS E9"),
            "ABERDEEN CLASS ECHO NINER".to_string()
        );
        assert_eq!(
            phoneticize("ATLANTA CLASS B AREA A"),
            "ATLANTA CLASS BRAVO AREA ALPHA".to_string()
        );
        assert_eq!(
            phoneticize("AIRBURST X SR-SS TUE-SAT and BY NOTAM 128.37"),
            "AIRBURST X-RAY SR-SS TUE-SAT and BY NOTAM ONE TWO EIGHT . TREE SEVEN".to_string()
        );
        assert_eq!(
            phoneticize("R2510B BY NOTAM 128.6"),
            "ROMEO TWO FIFE ONE ZERO BRAVO BY NOTAM ONE TWO EIGHT . SIX".to_string()
        );
    }
}
