
# Buster

Buster is a companion app to Microsoft Flight Simulator 2020 that alerts you
whenever you fly into a new airspace.

It shows your current airspace, and announces transitions into new airspaces
using text-to-speech.

You can [download the Windows 64-bit
binary](https://gitlab.com/jjwiseman/buster/-/jobs/1227437264/artifacts/download?file_type=archive).

The binary zip above includes airspace definitions for the United States. If you
want to fly in other countries, just [download the appropriate files from
OpenAIP](http://www.openaip.net/customer_export_akfshb9237tgwiuvb4tgiwbf/). (Why
does OpenAIP not link to that page anywhere? I had to get it from the forums.)
The files you want start with the 2-letter ISO country code for the region
you're interested in, then `_asp.aip`. After downloading the file, move it to
the folder containing `buster.exe`, and then restart `buster.exe`.

![MSFS 2020 screenshot](screenshot-fs2020-small.jpg?raw=true "MSFS 2020 screenshot")

![Buster screenshot](screenshot-buster.png?raw=true "Buster screenshot")


## Releases

### v0.1.0

First release.

### v0.1.1

April 30, 2021

Fixed an issue that might prevent it from running for a lot of people (changed
the build to include the msvc crt).
