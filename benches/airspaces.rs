use buster::airspaces::{load_airspace_data, AirspaceIndex};
use criterion::{criterion_group, criterion_main, Criterion};
use geo::Coordinate;

fn criterion_benchmark(c: &mut Criterion) {
    let airspaces = load_airspace_data("./openaip_airspace_united_states_us.aip").unwrap();
    let mut index = AirspaceIndex::new();
    index.add_airspaces(airspaces);
    let p = Coordinate {
        x: -118.19,
        y: 34.13,
    };
    c.bench_function("airspace index lookup", |b| {
        // let plane = SimPlaneState {
        //     lat: 34.13,
        //     lon: -118.19,
        //     alt: 100.,
        //     agl: 100.,
        // };
        b.iter(|| {
            index.get_at_coord(&p);
        })
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
