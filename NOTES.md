# Project notes

## April 29, 2021

Some things I'm not happy with:

* druid
* airspace file handling

### druid

It's way too complicated for this, and also kind of limited. I want to rewrite
the GUI using maybe https://github.com/gabdube/native-windows-gui.

### Airspaces

There aren't that many of them at
http://www.openaip.net/customer_export_akfshb9237tgwiuvb4tgiwbf; I could just
download them all and distribute them? Compress them if it even matters (I don't
think it really does).

If I included all available airspace files I probably wouldn't want to load them
all at runtime though, right? I guess it's not really a big deal—The rtree would
handle them just fine, and it wouldn't be that much memory. (One way to save
some memory would be to create an airspace type that just held the info I cared
about.)
